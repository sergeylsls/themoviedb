//
//  MovieSrv.swift
//  Themoviedb
//
//  Created by Sergey Leskov on 8/10/19.
//  Copyright © 2019 Sergey Leskov. All rights reserved.
//

import Foundation

class MovieSrv:Codable {
    var id: Int64 = 0
    var title: String?
    var popularity: Double?
    var posterPath: String?
    var overview: String?
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case popularity
        case posterPath = "poster_path"
        case overview
    }
    
}

