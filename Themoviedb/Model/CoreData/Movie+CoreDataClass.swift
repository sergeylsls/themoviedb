//
//  Movie+CoreDataClass.swift
//  Themoviedb
//
//  Created by Sergey Leskov on 8/10/19.
//  Copyright © 2019 Sergey Leskov. All rights reserved.
//
//

import Foundation
import CoreData


public class Movie: NSManagedObject {
    class override func entityName() -> String {
        return "Movie"
    }
    
    var printName: String {
        let name = self.title ?? ""
        return name
    }
    
    
    func update(id: Int64? = nil,
                title: String? = nil,
                popularity: Double? = nil,
                posterPath: String? = nil,
                overview: String? = nil,
                photo: NSData? = nil) {
        let isFirstUpdate = (self.title == nil ? true : false)
        let printText = isFirstUpdate ? "NEW" : "UPDATE"
        
        if let title = title { self.title = title }
        if let id = id { self.id = id }
        if let popularity = popularity { self.popularity = popularity }
        if let posterPath = posterPath { self.posterPath = posterPath }
        if let overview = overview { self.overview = overview }
        if let photo = photo { self.photo = photo }
        
        print("🎬 \(Movie.entityName()) \(printText): \(self.printName)")
    }
    
    
    func update(movieSrv: MovieSrv) {
        self.update(id: Int64(movieSrv.id),
                    title: movieSrv.title,
                    popularity: movieSrv.popularity,
                    posterPath: movieSrv.posterPath,
                    overview: movieSrv.overview)
    }
 
    

}
