//
//  Movie+CoreDataProperties.swift
//  Themoviedb
//
//  Created by Sergey Leskov on 8/10/19.
//  Copyright © 2019 Sergey Leskov. All rights reserved.
//
//

import Foundation
import CoreData


extension Movie {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Movie> {
        return NSFetchRequest<Movie>(entityName: "Movie")
    }

    @NSManaged public var popularity: Double
    @NSManaged public var title: String?
    @NSManaged public var id: Int64
    @NSManaged public var posterPath: String?
    @NSManaged public var overview: String?
    @NSManaged public var photo: NSData?

}
