//
//  MovieManager.swift
//  Themoviedb
//
//  Created by Sergey Leskov on 8/10/19.
//  Copyright © 2019 Sergey Leskov. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class MovieManager {
    
    static func getCount(moc: NSManagedObjectContext = CoreDataManager.shared.viewContext) -> Int {
        
        let request = NSFetchRequest<Movie>(entityName: Movie.entityName())
        
        let resultsArray = try? moc.fetch(request)
        
        return resultsArray?.count ?? 0
    }
    
    
    static func getByID(id: Int64?,
                        moc: NSManagedObjectContext = CoreDataManager.shared.viewContext ) -> Movie? {
        guard let id = id else { return nil}
        let request = NSFetchRequest<Movie>(entityName:Movie.entityName())
        
        
        var arrayPredicate: [NSPredicate] = []
        arrayPredicate.append(NSPredicate(format: "id == %i", id))
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: arrayPredicate)
        request.predicate = predicate
        
        let resultsArray = try? moc.fetch(request)
        
        return resultsArray?.first ?? nil
    }
    
    static func deleteRecords() {
        let request = NSFetchRequest<Movie>(entityName:Movie.entityName())
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: request as! NSFetchRequest<NSFetchRequestResult>)
        do {
            try CoreDataManager.shared.viewContext.execute(batchDeleteRequest)
        }
        catch {
            print(error)
        }
    }
    
    
    static func getImage(movie: Movie,  completion: @escaping (_ image: UIImage?) -> Void)  {
        if let photo = movie.photo {
  
            completion(UIImage(data: photo as Data))
            
        } else {
            
            if let posterPath = movie.posterPath,
                let url = URL(string: API.buildImagePath(poster_path: posterPath)),
                let data = try? Data(contentsOf: url),
                let image = UIImage(data: data)
            {
                let moc = CoreDataManager.shared.newBackgroundContext
                if let movieMOC = moc.object(with: movie.objectID) as? Movie {
                    movieMOC.update(photo: data as NSData)
                    CoreDataManager.shared.save(context: moc)
                }
           
                completion(image)
            } else {
               completion(nil)
            }
            
        }
    }
    
    
}
