//
//  NetworkService.swift
//  ParkingSystem
//
//  Created by Sergey Leskov on 8/14/18.
//  Copyright © 2018 Sergey Leskov. All rights reserved.
//

import Foundation
import Alamofire
import UIKit


protocol NetworkServiceDelegate: class {
    func dataLoaded(data:Any, typeQuery: TypeQuery)
    func errorLoadingData(error: String?, typeQuery: TypeQuery)
}


struct API {
    
    static let server = "https://api.themoviedb.org/3"
    static let imageServer = "https://image.tmdb.org/t/p/"
    
    static func buildImagePath(poster_path:String)->String{
        return "\(imageServer)w500\(poster_path)"
    }
    static func buildImagePathX3(poster_path:String)->String{
        return "\(imageServer)w1000\(poster_path)"
    }

    //movie
    static let moviePopular =  server + "/movie/popular"
}


class NetworkService {
    weak var delegate: NetworkServiceDelegate?
    
    //callWS
    private func callWS(urlString: String,
                        method: HTTPMethod,
                        parameters: [String: Any]? = nil,
                        typeQuery: TypeQuery,
                        encoding: ParameterEncoding) {
        guard let delegate = self.delegate else {
            print("Error!. [NetworkService] -> delegate = nil")
            return
        }
        
        
        let headers: HTTPHeaders = [
            //            "Content-Type": "application/json",
            //            "Authorization": "Bearer "+token,
            //            "Localization": Locale.current.languageCode ?? "en",
            //            "cache-control": "no-cache"
            :]
        
        
        guard let url = URL(string: urlString) else {
            print("🧨 Error!. [NetworkService] -> URL(string: urlString)")
            delegate.errorLoadingData(error: nil, typeQuery: typeQuery)
            return
        }
        
        
        let req = request(url,
                          method: method,
                          parameters: parameters,
                          encoding: encoding,
                          headers: headers)
        
        // debug begin
        //let isDebug = true
        let isDebug = false
        if isDebug {
            req.response { (response) in
                if let error = response.error  {
                    print("🧨 Error!. [NetworkService] -> \(error)")
                    return
                }
                print(response.request!)
                print(response.response!)
                
                if let data = response.data {
                    if let str = String(data: data, encoding: String.Encoding.ascii) {
                        print(str)
                    }
                }
                
            }
            //            delegate.errorLoadingData(error: nil, typeQuery: typeQuery)
            //            return
        }
        // debug end
        
        
        
        req.responseJSON { response in
            if let error = response.result.error  {
                delegate.errorLoadingData(error: error.localizedDescription, typeQuery: typeQuery)
                return
            }
            
            guard let dict = response.result.value as? [String: Any] else {
                print("🧨 Error!. [NetworkService] -> json = response.result.value as? [String: Any]")
                delegate.errorLoadingData(error: nil, typeQuery: typeQuery)
                return
            }
            
            if let status = dict["success"] as? Bool,
                let statusMessage = dict["status_message"] as? String,
                status == false {
                delegate.errorLoadingData(error: statusMessage, typeQuery: typeQuery)
                return
            }
            
            delegate.dataLoaded(data: dict, typeQuery: typeQuery)
        }
    }
}


//==================================================
// MARK: - movie
//==================================================
extension NetworkService {
    
    
    func getMoviePopular(page: Int)  {
        
        let parameters: Parameters = [
            "api_key": Constants.Settings.apiKey,
            "language": "en-US",
            "page": page
        ]
        
        self.callWS(urlString: API.moviePopular, method: .get,  parameters: parameters, typeQuery: .moviePopular, encoding: URLEncoding.default)
    }
    
}


