//
//  PopularTableVC.swift
//  Themoviedb
//
//  Created by Sergey Leskov on 8/9/19.
//  Copyright © 2019 Sergey Leskov. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class PopularTableVC: BaseFetchTableVC {
    
    @IBOutlet weak var activityBottomUI: UIActivityIndicatorView!
    @IBOutlet weak var labelNetworkUI: UILabel!
    
    //==================================================
    // MARK: - Fetch Data
    //==================================================
    private var _fetchController: NSFetchedResultsController<NSFetchRequestResult>?
    internal override var fetchController: NSFetchedResultsController<NSFetchRequestResult>! {
        get {
            if _fetchController == nil {
                let fetchRequest: NSFetchRequest<NSFetchRequestResult> = Movie.fetchRequest()
                
                
                let sortDescriptor1 = NSSortDescriptor(key: "popularity", ascending: false)
                fetchRequest.sortDescriptors = [sortDescriptor1]
                
                let resultController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: CoreDataManager.shared.viewContext,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
                resultController.delegate = self
                
                _fetchController = resultController
            }
            return _fetchController
        }
        set(newValue) {
            _fetchController = newValue
        }
    }
    
    fileprivate var pageLast: Int = 0
    fileprivate var isRequestStart = false
    
    
    fileprivate let networkService = NetworkService()
    fileprivate let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.apple.com")
    fileprivate let cellMovieTableCell = "MovieTableCell"
    fileprivate let segieDetail = "Detail"
    
    //==================================================
    // MARK: - General
    //==================================================
    override func viewDidLoad() {
        MovieManager.deleteRecords()
        
        super.viewDidLoad()
        self.networkService.delegate  = self
        self.tableView.register(UINib(nibName: self.cellMovieTableCell, bundle: nil), forCellReuseIdentifier: self.cellMovieTableCell)
        
        self.activityBottomUI.stopAnimating()
        
        
        let isReachable = self.reachabilityManager?.isReachable ?? true
        if isReachable {
            self.requestFirstData()
        } else {
            self.labelNetworkUI.isHidden = false
            self.labelNetworkUI.text = NSLocalizedString("Waiting for internet connection", comment: "")
            self.listenForReachability()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = NSLocalizedString("Popular movies", comment: "")
        
    }
    
    
    fileprivate func requestData(page: Int) {
        if isRequestStart { return }
        self.isRequestStart = true
        self.networkService.getMoviePopular(page: page)
    }
    
    fileprivate func requestFirstData() {
        self.startLoader()
        self.requestData(page: 1)
        self.labelNetworkUI.isHidden = true
    }
    
    
    //==================================================
    // MARK: - func
    //==================================================
    
    func listenForReachability() {
        self.reachabilityManager?.listener = { [weak self] status in
            switch status {
            case .notReachable:
                print("The network is not reachable")
                
            case .unknown :
                print("It is unknown whether the network is reachable")
                
            case .reachable(.ethernetOrWiFi), .reachable(.wwan):
                self?.reachabilityManager?.stopListening()
                self?.requestFirstData()
            }
        }
        self.reachabilityManager?.startListening()
        
    }
    
    
    //==================================================
    // MARK: - Navigation
    //==================================================
    internal override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.segieDetail  {
            let destinationController = segue.destination as! DetailVC
            destinationController.movie = sender as? Movie
        }
        
    }
    
}


//==================================================
// MARK: - UITableViewDataSource, UITableViewDelegate
//==================================================
extension PopularTableVC {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let count = MovieManager.getCount()
        if indexPath.row == count-1 {
            self.activityBottomUI.startAnimating()
            self.requestData(page: self.pageLast + 1)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellMovieTableCell, for: indexPath) as! MovieTableCell
        let movie = self.fetchController.object(at: indexPath) as? Movie
        cell.movie = movie
        cell.reloadData()
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = self.fetchController.object(at: indexPath) as? Movie
        performSegue(withIdentifier: self.segieDetail, sender: movie)
    }
    
}


//==================================================
// extension - NetworkServiceDelegate
//==================================================
extension PopularTableVC: NetworkServiceDelegate {
    func errorLoadingData(error: String?, typeQuery: TypeQuery) {
        self.stopLoader()
        self.activityBottomUI.stopAnimating()
        self.isRequestStart = false
        if let error = error {
            MessagerManager.showError(message: error)
        }
    }
    
    func dataLoaded(data: Any, typeQuery: TypeQuery) {
        self.stopLoader()
        self.activityBottomUI.stopAnimating()
        self.isRequestStart = false
        if typeQuery == .moviePopular {
            
            guard let dict = data as? [String: Any],
                let page = dict["page"] as? Int else { return }
            
            guard let results = dict["results"] as? [[String: Any]],
                let json = try? JSONSerialization.data(withJSONObject: results, options: []),
                let movieSrvArray = try? JSONDecoder().decode([MovieSrv].self, from: json) else { return }
            
            self.pageLast = page
            
            for movieSrv in movieSrvArray {
                let moc = CoreDataManager.shared.newBackgroundContext
                
                let movie: Movie?
                if let old = MovieManager.getByID(id: movieSrv.id, moc: moc) {
                    movie = old
                } else {
                    let new = Movie.newObject(moc: moc) as? Movie
                    movie = new
                }
                if let movie = movie {
                    movie.update(movieSrv: movieSrv)
                    CoreDataManager.shared.save(context: moc)
                }
            }
        }
    }
}
