//
//  BaseFetchTableVC.swift
//  ParkingSystem
//
//  Created by Sergey Leskov on 8/20/18.
//  Copyright © 2018 Sergey Leskov. All rights reserved.
//

import UIKit
import CoreData

class BaseFetchTableVC: BaseFetchVC {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet{
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    
}

//==================================================
// MARK: - UITableViewDataSource, UITableViewDelegate
//==================================================

extension BaseFetchTableVC: UITableViewDataSource, UITableViewDelegate {
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        if let sections = fetchController.sections {
            return sections.count
        }
        return 0
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchController.sections {
            let currentSection = sections[section]
            return currentSection.numberOfObjects
        }
        return 0
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "None", for: indexPath)
        assert(false, "Subclasses must override the method")
        return cell
    }
    
 }


//==================================================
// MARK: - BaseFetchTableViewController
//==================================================

extension BaseFetchTableVC {
    final override func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        //        print("[BaseFetchTableVC] controllerWillChangeContent:")
        tableView.beginUpdates()
    }
    
    final override func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        //print("[BaseFetchTableVC] controller:didChange:atSectionIndex[\(String(describing: sectionIndex))]:for[\(String(describing: type))]")
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        @unknown default:
            break
        }
    }
    
    final override func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            //            print("[BaseFetchTableVC] controller:didChange:at[\(String(describing: indexPath))]:for[insert]:newIndexPath[\(String(describing: newIndexPath))]:")
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            //            print("[BaseFetchTableVC] controller:didChange:at[\(String(describing: indexPath))]:for[delete]:newIndexPath[\(String(describing: newIndexPath))]:")
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            //            print("[BaseFetchTableVC] controller:didChange:at[\(String(describing: indexPath))]:for[update]:newIndexPath[\(String(describing: newIndexPath))]:")
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            //            print("[BaseFetchTableVC] controller:didChange:at[\(String(describing: indexPath))]:for[move]:newIndexPath[\(String(describing: newIndexPath))]:")
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        @unknown default:
            break
        }
    }
    
    
    override func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        //        print("[BaseFetchTableVC] controllerDidChangeContent:")
        //  tableView.reloadData()
        tableView.endUpdates()
    }
    
}
