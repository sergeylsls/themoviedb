//
//  BaseVC.swift
//  ParkingSystem
//
//  Created by Sergey Leskov on 8/13/18.
//  Copyright © 2018 Sergey Leskov. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BaseVC: UIViewController, NVActivityIndicatorViewable {
    
    //==================================================
    // MARK: - General
    //==================================================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //==================================================
    // MARK: - Func
    //==================================================
    public func stopLoader()  {
        self.stopAnimating()
    }
    
    public func startLoader(message: String? = nil)  {
        //https://github.com/ninjaprox/NVActivityIndicatorView
        self.startAnimating(message: message,  type: .ballClipRotatePulse)
        
    }
    
    
    
    
}


