//
//  BaseFetchVC.swift
//  ParkingSystem
//
//  Created by Sergey Leskov on 8/20/18.
//  Copyright © 2018 Sergey Leskov. All rights reserved.
//

import UIKit
import CoreData

class BaseFetchVC: BaseVC {
    
    internal var fetchController: NSFetchedResultsController<NSFetchRequestResult>! {
        get {
            assert(false, "Subclasses must override the method")
            return NSFetchedResultsController()
        }
        set(newValue) {
            assert(false, "Subclasses must override the method")
        }
    }
    
    //==================================================
    // MARK: - General
    //==================================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.performFetch()
    }
    
    //==================================================
    // MARK: - Func
    //==================================================
    fileprivate func performFetch() {
        //        print("[BaseFetchVC] performFetch")
        //NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: nil)
        do {
            try fetchController.performFetch()
            //print(fetchController.fetchedObjects?.count)
        } catch {
            print(error)
        }
    }    
}

//==================================================
// MARK: - BaseFetchTableViewController
//==================================================

extension BaseFetchVC: NSFetchedResultsControllerDelegate {
    internal func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        assert(false, "Subclasses must override the method")
    }
    
    internal func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        assert(false, "Subclasses must override the method")
    }
    
    internal func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        assert(false, "Subclasses must override the method")
    }
    
    internal func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        assert(false, "Subclasses must override the method")
    }
}
