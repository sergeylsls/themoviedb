//
//  DetailVC.swift
//  Themoviedb
//
//  Created by Sergey Leskov on 8/10/19.
//  Copyright © 2019 Sergey Leskov. All rights reserved.
//

import UIKit
import AlamofireImage


class DetailVC: BaseVC {
    
    @IBOutlet weak var scrollViewUI: UIScrollView!
    @IBOutlet weak var imageUI: UIImageView!
    @IBOutlet weak var labelTitleUI: UILabel!
    @IBOutlet weak var labelTextUI: UILabel!
    
    var movie: Movie?
    
    //==================================================
    // MARK: - General
    //==================================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let movie = self.movie {
            self.labelTitleUI.text = movie.title
            self.labelTextUI.text = movie.overview
            if let posterPath = movie.posterPath,
                let url = URL(string: API.buildImagePath(poster_path: posterPath)) {
                self.imageUI.af_setImage(withURL: url, imageTransition: .crossDissolve(0.5))
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = NSLocalizedString("Detail", comment: "")
    }
    
    
    
}
