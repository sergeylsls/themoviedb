//
//  NSManagedObject.swift
//  uzer
//
//  Created by Sergey Leskov on 3/26/18.
//  Copyright © 2018 Uzer. All rights reserved.
//

import Foundation
import CoreData


extension NSManagedObject {
    
    //entityName
    @objc class func entityName() -> String {
        assert(false, "Subclasses must override the method")
        return ""
    }

    //newObject
    public class func newObject(moc: NSManagedObjectContext) -> NSManagedObject? {
        var resultEntity: NSManagedObject?
        resultEntity = NSEntityDescription.insertNewObject(forEntityName: self.entityName(), into: moc)
        return resultEntity
    }

}
