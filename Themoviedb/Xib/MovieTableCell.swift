//
//  MovieTableCell.swift
//  Themoviedb
//
//  Created by Sergey Leskov on 8/10/19.
//  Copyright © 2019 Sergey Leskov. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieTableCell: UITableViewCell {
    
    @IBOutlet weak var viewShadowUI: UIView!
    @IBOutlet weak var viewMainUI: UIView!
    @IBOutlet weak var imageUI: UIImageView!
    @IBOutlet weak var labelTitleUI: UILabel!
    @IBOutlet weak var labelTextUI: UILabel!
    @IBOutlet weak var activityUI: UIActivityIndicatorView!
    
    
    var movie: Movie?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewMainUI.layer.cornerRadius = 10
        
        //border
        self.viewMainUI.layer.borderColor = UIColor.lightGray.cgColor
        self.viewMainUI.layer.borderWidth = 1
        
        
        //shadow
        self.viewShadowUI.layer.shadowColor = UIColor.lightGray.cgColor
        self.viewShadowUI.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.viewShadowUI.layer.shadowRadius = 3.0
        self.viewShadowUI.layer.shadowOpacity = 0.5
        self.viewShadowUI.layer.masksToBounds = false
        
        self.labelTitleUI.text = ""
        self.labelTextUI.text = ""
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    public func reloadData() {
        guard let movie = self.movie else { return }
        
        self.labelTitleUI.text = movie.title
        self.labelTextUI.text = movie.overview
        
        self.activityUI.startAnimating()
        if let posterPath = movie.posterPath,
            let url = URL(string: API.buildImagePath(poster_path: posterPath)) {
            self.imageUI.af_setImage(withURL: url, imageTransition: .crossDissolve(0.5))
            self.activityUI.stopAnimating()
        }

        
    }
    
    
}
